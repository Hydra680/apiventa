"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.getCarritoUserValidate = exports.getCarritoUserNotValidate = exports.addCarrito = exports.addProductCarrito = void 0;
const database_1 = require("../database");
exports.addProductCarrito = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const { id_carrito, id_producto, cantidad } = req.body;
    console.log(id_carrito, id_producto, cantidad);
    const fecha = new Date();
    yield database_1.pool.query('insert into carrito_add_producto (id_carrito, id_producto, cantidad_reservas, fecha_limite, fecha_inicio_reserva)values ($1, $2 ,$3,$4,$5)', [id_carrito, id_producto, cantidad, fecha.setDate(fecha.getDate() + 2), fecha])
        .then((data) => {
        return res.status(200).json({ message: "Insert to Product Correct..." });
    })
        .catch((err) => {
        if (err) {
            return res.status(400).json({ message: "Internal Server Error" });
        }
    });
});
exports.addCarrito = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const fecha = new Date();
    console.log(fecha);
    const id = req.params.id;
    yield database_1.pool.query('insert into Carrito(id_usuario, estado_validacion_compra, fecha_validacion)values ($1,$2,$3)', [id, false, fecha])
        .then((data) => {
        if (data) {
            res.status(200).json({ message: "carrito creado con exito" });
        }
    })
        .catch((err) => {
        if (err) {
            return res.status(400).json({ message: "Internal Server Error", err });
        }
    });
});
exports.getCarritoUserNotValidate = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = req.params.id;
    console.log(id);
    yield database_1.pool.query('select  p.* from carrito c, usuario u ,carrito_add_producto cr,producto p where u.id_usuario=$1 and cr.id_carrito = c.id_carrito and cr.id_producto = p.id_producto and  c.id_usuario = u.id_usuario  and estado_validacion_compra = $2', [id, false])
        .then((data) => {
        if (data) {
            //return data.rows;
            return res.status(200).json(data.rows);
        }
    })
        .catch((err) => {
        if (err) {
            return res.status(400).json({ message: "Internal Server Error", err });
        }
    });
});
exports.getCarritoUserValidate = (req, res) => __awaiter(void 0, void 0, void 0, function* () {
    const id = req.params.id;
    console.log(id);
    yield database_1.pool.query('select  c.id_carrito, p.* from carrito c, usuario u ,carrito_add_producto cr,producto p where u.id_usuario=$1 and cr.id_carrito = c.id_carrito and cr.id_producto = p.id_producto and  c.id_usuario = u.id_usuario  and estado_validacion_compra = $2', [id, false])
        .then((data) => {
        if (data) {
            //return data.rows;
            return res.status(200).json(data.rows);
        }
    })
        .catch((err) => {
        if (err) {
            return res.status(400).json({ message: "Internal Server Error", err });
        }
    });
});
//# sourceMappingURL=controller.carrito.js.map