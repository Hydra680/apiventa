"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
exports.sumarDias = void 0;
function sumarDias(fecha, dias) {
    fecha.setDate(fecha.getDate() + dias);
    return fecha;
}
exports.sumarDias = sumarDias;
//# sourceMappingURL=ControlDiasCarrito.js.map