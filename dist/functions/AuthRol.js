"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.IsAdmin = void 0;
const database_1 = require("../database");
exports.IsAdmin = (req) => __awaiter(void 0, void 0, void 0, function* () {
    //console.log(req.userId);
    try {
        const result = yield database_1.pool.query("SELECT * FROM Usuario WHERE id_usuario = $1", [req.userId]);
        //console.log(result.rows)
        const passwordCrypt = result.rows.map((data) => { return data; });
        //console.log(passwordCrypt[0].id_usuario);
        return (passwordCrypt[0].rol_usuario == "Admin") ? true : false;
    }
    catch (error) {
        //console.log(error);
        return false;
    }
    return false;
});
//# sourceMappingURL=AuthRol.js.map