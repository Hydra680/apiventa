"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
exports.TokenValidator = void 0;
const jsonwebtoken_1 = __importDefault(require("jsonwebtoken"));
exports.TokenValidator = (req, res, next) => {
    const token = req.header('auth-token');
    if (!token) {
        return res.status(401).json('acess denied');
    }
    const payload = jsonwebtoken_1.default.verify(token, "TOkEN_TESTING");
    //console.log(payload);
    req.userId = payload.id;
    next();
};
//# sourceMappingURL=veryfiToken.js.map