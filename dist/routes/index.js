"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = require("express");
const routes_usuario_1 = __importDefault(require("../routes/routes.usuario"));
const routes_producto_1 = __importDefault(require("../routes/routes.producto"));
const auth_1 = __importDefault(require("../routes/auth"));
const route_carrito_1 = __importDefault(require("../routes/route.carrito"));
const router = express_1.Router();
//router.get('/',(res:Response)=>{return res.send("WelcomeToApiVentasLeyvaFashion")});
router.use(auth_1.default);
router.use(routes_usuario_1.default);
router.use(routes_producto_1.default);
router.use(route_carrito_1.default);
exports.default = router;
//# sourceMappingURL=index.js.map