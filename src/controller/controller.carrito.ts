import { Response, Request, response } from "express";
import { pool } from '../database';
import { QueryResult } from 'pg';
import { IsAdmin } from "../functions/AuthRol";

export const addProductCarrito = async (req: Request, res: Response)=>{
    
    const {id_carrito, id_producto,cantidad} = req.body;
    console.log(id_carrito, id_producto,cantidad);
    const fecha = new Date();
    await pool.query('insert into carrito_add_producto (id_carrito, id_producto, cantidad_reservas, fecha_limite, fecha_inicio_reserva)values ($1, $2 ,$3,$4,$5)',[id_carrito, id_producto,cantidad,fecha.setDate(fecha.getDate() + 2),fecha])
    .then(
        (data)=>{
            return res.status(200).json({message:"Insert to Product Correct..."})
        }
    )
    .catch((err)=>{
        if(err){
            return res.status(400).json({message:"Internal Server Error"})
        }
    });
}
export const addCarrito = async (req: Request, res: Response)=>{
    const fecha = new Date();
    console.log(fecha);
    const id = req.params.id;
    await pool.query('insert into Carrito(id_usuario, estado_validacion_compra, fecha_validacion)values ($1,$2,$3)',[id,false,fecha])
    .then(
        (data)=>{
            if(data){
                res.status(200).json({message:"carrito creado con exito" })
            }
        }
    )
    .catch((err)=>{
        if(err){
            return res.status(400).json({message:"Internal Server Error",err})
        }
    });
};

export const getCarritoUserNotValidate = async (req: Request, res: Response) => {
    const id = req.params.id;
    console.log(id);
        await pool.query('select  p.* from carrito c, usuario u ,carrito_add_producto cr,producto p where u.id_usuario=$1 and cr.id_carrito = c.id_carrito and cr.id_producto = p.id_producto and  c.id_usuario = u.id_usuario  and estado_validacion_compra = $2',[id,false])
        .then((data)=>{
            if(data){
                //return data.rows;
                return res.status(200).json(data.rows)
            }

        })
        .catch((err)=>{
            if(err){
                return res.status(400).json({message:"Internal Server Error",err})
            }
        });

}

export const getCarritoUserValidate = async (req: Request, res: Response) => {
    const id = req.params.id;
    console.log(id);
    await pool.query('select  c.id_carrito, p.* from carrito c, usuario u ,carrito_add_producto cr,producto p where u.id_usuario=$1 and cr.id_carrito = c.id_carrito and cr.id_producto = p.id_producto and  c.id_usuario = u.id_usuario  and estado_validacion_compra = $2',[id,false])
    .then((data)=>{
        if(data){
            //return data.rows;
            return res.status(200).json(data.rows)
        }

    })
    .catch((err)=>{
        if(err){
            return res.status(400).json({message:"Internal Server Error",err})
        }
    });
}