import { Response, Request, response } from "express";
import { pool } from '../database';
import { QueryResult } from 'pg';
import { IsAdmin } from "../functions/AuthRol";

export const getProduct = async (req: Request, res: Response): Promise<Response> => {
    try {
        const response: QueryResult<any> = await pool.query('SELECT * FROM producto');
        if (response.rowCount == 0) {
            return res.status(400).json("No Existen datos registrados");
        } else
            return res.status(200).json(response.rows);

    } catch (error) {
        console.log(error);
        return res.status(500).json("internal server error");
    }

}
export const getproductcategory = async (req: Request, res: Response): Promise<Response> => {
    //console.log(req.headers.categoria);
    const categoria = req.headers.categoria;
    try {
        const response: QueryResult<any> = await pool.query('SELECT * FROM producto where categoria like $1', [
            categoria
        ]);
        if (response.rowCount == 0) {
            return res.status(400).json("No se encontraron datos");
        } else
            return res.status(200).json(response.rows);

    } catch (error) {
        console.log(error);
        return res.status(500).json("internal server error");
    }
};

export const addProduct = async (req: Request, res: Response) => {

    const sw = await IsAdmin(req);
    if (sw) {
        //console.log("Admin")
            const { nombre, descripcion, cantidad_productos, fecha_Registro, categoria, url_image_product } = req.body;
            await pool.query(
                "insert into Producto(nombre, descripcion, cantidad_productos, fecha_Registro,categoria, url_image_product) values($1,$2,$3,$4,$5,$6)",
                [nombre, descripcion, cantidad_productos, fecha_Registro, categoria, url_image_product]
            ).then(
               async (data)=>{
                    await pool.query("SELECT * FROM Producto WHERE nombre = $1 and descripcion= $2 and cantidad_productos= $3",[nombre,descripcion,cantidad_productos])
                    .then(
                        async(data)=>{
                            const product = data.rows.map(
                                (data)=>{
                                    //console.log(data);
                                    //console.log(data.id_producto);
                                    return data;
                                }
                            );
                            return res.status(200).json( product );
                        }
                        
                    ).catch(
                        (err)=>{
                            return res.status(400).json({
                                message: "Internal Server Error",err
                            }); 
                        }
                    );

                }
            ).catch(
                (err)=>{
                    return res.status(400).json({
                        message: "Internal Server Error",err
                    }); 
                }
            );
           
    } else {
        console.log("User");
        return res.status(400).json({
            message: "Acces Denied",
        });
    }
}

export const addTalla = async (req: Request, res: Response): Promise<Response> => {
    const id = req.params.id;
    const { talla } = req.body;

    console.log(talla, id);
    const sw = await IsAdmin(req);
    if (sw) {
        try {
            const respProduct: QueryResult = await pool.query("SELECT * FROM Producto Where id_producto = $1", [id]);
            if (respProduct.rowCount > 0) {
                try {
                    const resp: QueryResult = await pool.query("insert into Talla(id_producto, talla_producto)values ($1, $2)", [id, talla]);
                    console.log("*********************************************");
                    console.log(respProduct.rows[0]);
                    console.log("*********************************************");
                    // ! Observed retunr data insert 
                    return res.status(200).json(
                        {
                            message: "Insert Correct",
                            data: respProduct.rows[0]
                        }
                    );
                } catch (error) {
                    return res.status(400).json({
                        message: "Internal Server Error",
                        error
                    });
                }
            } else {
                return res.status(404).json({
                    message: "No Existe el Producto",
                });
            }

        } catch (error) {
            return res.status(400).json({
                message: "Internal Server Error",
                error
            });
        }
    } else {
        console.log("user");
        return res.status(400).json({
            message: "Internal Server Error",
        });
    }

}


export const addPrecioColor = async (req: Request, res: Response): Promise<Response> => {
    const { id_producto, id_talla, color, precio_producto } = req.body;
    console.log(id_producto, id_talla, color, precio_producto);
    const sw = await IsAdmin(req);
    if (sw) {
        try {
            const respProduct: QueryResult = await pool.query("SELECT * FROM Producto WHERE id_producto = $1", [id_producto]);
            if (respProduct.rowCount > 0) {
                try {

                    const resTalla: QueryResult = await pool.query("select * from Producto p, Talla t where p.id_producto = $1 and  t.id_talla = $2 and p.id_producto = t.id_producto ", [id_producto, id_talla]);
                    if (resTalla.rowCount > 0) {
                        try {
                            const response: QueryResult = await pool.query("insert into Color(id_producto, id_talla, color, precio_producto) values ($1, $2,$3,$4);", [id_producto, id_talla, color, precio_producto]);
                            console.log(response);
                            //return res.status(200).json(response.rows[0]);
                            return res.status(200).json({ Mesage: "insert correct" });
                        } catch (error) {

                            return res.status(400).json({ message: "internal server error", err: error });
                        }

                    } else
                        return res.status(404).json({
                            message: "data Talla not Foud"
                        });

                } catch (error) {
                    return res.status(400).json({ error: "internal Server Error" });
                }
            } else {
                return res.status(404).json({
                    message: "data Product not Foud"
                });
            }

        } catch (error) {
            return res.status(400).json("Internal Server Error");
        }
    } else {
        return res.send("Acces Denied");
    }
}

export const EditTPrecio = async (req: Request, res: Response) => {
    const id_producto = req.params.id_producto;
    const precio_producto = req.body.precio_producto;
    console.log(id_producto, precio_producto);
    //const id_talla = req.query.id_talla;
    //console.log("editar precio");
    const sw = await IsAdmin(req);
    if (sw) {
        try {

            const resp: QueryResult = await pool.query("select * from Producto p where p.id_producto = $1 ", [id_producto]);
            if (resp.rowCount > 0) {
                try {
                    const editresp: QueryResult = await pool.query("UPDATE color SET precio_producto = $2 WHERE id_producto = $1", [id_producto, precio_producto]);
                    return res.status(200).json({ message: "Update Correct..." });
                } catch (error) {
                    return res.status(400).json({ message: "Internal Server Erro" });
                }
            } else
                return res.status(404).send({ message: "Data not found" });
        } catch (error) {
            return res.status(400).json({ message: "Internal server error" });
        }
    } else {
        return res.send("Acces denied");
    }

}
export const editProducto = async (req: Request, res: Response) => {

    const now = new Date();
    //console.log('La fecha actual es',now);
    const id_producto = req.params.id_producto;
    const sw = await IsAdmin(req);
    if (sw) {
        try {
            const resp: QueryResult = await pool.query("select * from Producto p where p.id_producto = $1 ", [id_producto]);
            if (resp.rowCount > 0) {
                try {
                    const { nombre, descripcion, cantidad_productos, categoria, url_image_product } = req.body;
                    const fecha_Registro = now;

                    const response: QueryResult = await pool.query(
                        "UPDATE Producto set nombre=$1, descripcion=$2, cantidad_productos=$3, fecha_Registro=$4,categoria=$5, url_image_product=$6 Where id_producto = $7",
                        [nombre, descripcion, cantidad_productos, fecha_Registro, categoria, url_image_product, id_producto]
                    );
                    return res.status(200).json({ message: "Update Correct..." });
                } catch (error) {
                    return res.status(400).json({ message: "Internal Server Error", err: error });
                }
            } else
                return res.status(404).send({ message: "Data not found" });
        } catch (error) {
            return res.status(400).json({ message: "Internal server error" });
        }
    } else {
        return res.send("Acces denied");
    }

}

// ! verificar Ruta
export const deleteProducto = async (req: Request, res: Response) => {
    const id_producto = req.params.id_producto;
    //console.log(id_producto);
    await pool.query("delete from Color where id_producto = $1", [id_producto])
        .then(
            async (data) => {
                if (data) {
                    await pool.query("delete from Talla where id_producto = $1", [id_producto])
                        .then(
                            async (datat) => {
                                await pool.query("delete from Producto where id_producto = $1", [id_producto])
                                    .then(
                                        (datp) => {
                                            res.status(200).json({ message: "Delete Correct..." });
                                        }
                                    )
                                    .catch((err) => {
                                        if (err) {
                                            res.status(400).json({ message: "Internal Server Error 3", err })
                                        }

                                    });
                            }
                        ).catch(
                            (err) => {
                                if (err) {
                                    res.status(400).json({ message: "Internal Server Error 2", err })
                                }
                            });
                }
            }
        )
        .catch(
            (err) => {
                if (err) {
                    res.status(400).json({ message: "Internal Server Error 1", err })
                }

            });
}