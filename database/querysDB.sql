CREATE TABLE Usuario(
	id_usuario            SERIAL PRIMARY KEY ,
	nombre               varchar(15) ,
	apellidos            varchar(30) ,
	correo               varchar(25) UNIQUE,
	pasword              varchar(15) ,
	rol_usuario          varchar(8) ,
	url_image            varchar(150) 
);
ALTER TABLE nombre_tabla ALTER COLUMN nombre_columna SET DATA TYPE tipo_de_dato;

insert into Usuario(nombre, apellidos, correo, pasword, rol_usuario, url_image)
values ('Benjamin','Cruz Q','hydra60@gmail.com','$2a$10$Hgtd/AZQuNeMfG6/5PrjteyiZGUmEX7FYapvxP7ht1obAMQdtr4P6', 'Admin','https://i.pinimg.com/474x/c6/94/8f/c6948f28bfc3757f0970e4cf5bf67126.jpg');

-- Administradores
insert into Usuario(nombre, apellidos, correo, pasword, rol_usuario, url_image)
values ('Pamela','Garcia','usuario1@gmail.com','123456789', 'Usuario','https://i.pinimg.com/474x/c6/94/8f/c6948f28bfc3757f0970e4cf5bf67126.jpg');

-- Usuarios
insert into Usuario(nombre, apellidos, correo, pasword, rol_usuario, url_image)
values ('Ximena','Tambo','usuario2@gmail.com','123456789', 'Usuario','https://i.pinimg.com/474x/c6/94/8f/c6948f28bfc3757f0970e4cf5bf67126.jpg');

insert into Usuario(nombre, apellidos, correo, pasword, rol_usuario, url_image)
values ('Pedro','Delgado','usuario3@gmail.com','123456789', 'Usuario','https://i.pinimg.com/474x/c6/94/8f/c6948f28bfc3757f0970e4cf5bf67126.jpg');

CREATE TABLE Carrito(
	id_carrito            SERIAL PRIMARY KEY,
	id_usuario            INTEGER NOT NULL ,
	estado_validacion_compra varchar(5) ,
	fecha_validacion      DATE,
    CONSTRAINT fk_id_usuario FOREIGN KEY (id_usuario) REFERENCES Usuario (id_usuario) 
);

-- usuario 2
insert into Carrito(id_usuario, estado_validacion_compra, fecha_validacion)
values (2,'false','2020/06/06');
insert into Carrito(id_usuario, estado_validacion_compra, fecha_validacion)
values (2,'true','2020/07/06');
insert into Carrito(id_usuario, estado_validacion_compra, fecha_validacion)
values (2,'false','2020/08/06');
-- usuario 3
insert into Carrito(id_usuario, estado_validacion_compra, fecha_validacion)
values (3,'true','2020/01/18');
-- usuario 4
insert into Carrito(id_usuario, estado_validacion_compra, fecha_validacion)
values (4,'false','2020/02/01');
insert into Carrito(id_usuario, estado_validacion_compra, fecha_validacion)
values (4,'false','2020/06/20');


CREATE TABLE Producto(
	id_producto           SERIAL PRIMARY KEY,
	nombre               varchar(50) ,
	descripcion          varchar(250) ,
	cantidad_productos    INTEGER ,
	fecha_Registro        DATE ,
	categoria            varchar(70) ,
    url_image_product     varchar(250)
);

-- Productos
insert into Producto(nombre, descripcion, cantidad_productos, fecha_Registro,categoria, url_image_product)
values ('Cartera de Cuero', 'marca guess, cuero ecologico, modelo clasico', 150,'2020/10/22','Accesorios', 'https://dafitistaticcl-a.akamaihd.net/p/guess-0360-996119-1-product.jpg');

insert into Producto(nombre, descripcion, cantidad_productos, fecha_Registro,categoria, url_image_product)
values ('Tacones de fiesta', 'marca Delicius, material gamusa,modelo con taco Alto', 250,'2020/10/22','Calzados', 'https://i.pinimg.com/736x/02/eb/f6/02ebf6e77a2a6d9c77f23ca36e49b594.jpg');

insert into Producto(nombre, descripcion, cantidad_productos, fecha_Registro,categoria, url_image_product)
values ('pantaletas', 'marca victoria secrets', 500,'2020/10/22','Lenceria','https://media.gotrendier.mx/media/products/8/2/6/2/0/5/1/n_150262820190331170332_1.jpg');

insert into Producto(nombre, descripcion, cantidad_productos, fecha_Registro,categoria, url_image_product)
values ('Blusa Formal', 'modelo formal, marca Joompy, material de seda y blusa de magas largas', 750,'2020/10/22','Blusas de Dama', 'https://ae01.alicdn.com/kf/Hcf2289b44c764fd7885ae66d6006dafbi.jpg');

insert into Producto(nombre, descripcion, cantidad_productos, fecha_Registro,categoria, url_image_product)
values ('Jeans semi licrado', 'modelo moderno, marca machine, material jean semi licrado', 150,'2020/10/22','Pantalones Casual Urbano','https://i.pinimg.com/originals/90/fa/72/90fa72c476867fb4addc18c33411b420.jpg'); -- mujer

CREATE TABLE Talla(
	id_talla              SERIAL PRIMARY KEY ,
	id_producto           SERIAL ,
	talla_producto       varchar(15) ,
    CONSTRAINT fk_id_productoT FOREIGN KEY (id_producto) REFERENCES Producto (id_producto) 
);
-- cartera de cuero
insert into Talla(id_producto, talla_producto)
values (1, 'mediano');
insert into Talla(id_producto, talla_producto)
values (1, 'pequeño');
insert into Talla(id_producto, talla_producto)
values (1, 'Grande');
-- Botas Bucaneras
insert into Talla(id_producto, talla_producto)
values (2, '5 1/2 ');
insert into Talla(id_producto, talla_producto)
values (2, '6 1/2 ');
insert into Talla(id_producto, talla_producto)
values (2, '7 1/2 ');
insert into Talla(id_producto, talla_producto)
values (2, '8 1/2 ');
insert into Talla(id_producto, talla_producto)
values (2, '9 1/2 ');
insert into Talla(id_producto, talla_producto)
values (2, '10 1/2 ');
-- pantaletas
insert into Talla(id_producto, talla_producto)
values (3, 'XS');
insert into Talla(id_producto, talla_producto)
values (3, 'S');
insert into Talla(id_producto, talla_producto)
values (3, 'M');
insert into Talla(id_producto, talla_producto)
values (3, 'L');
insert into Talla(id_producto, talla_producto)
values (3, 'XL');
--Blusa Formal 
insert into Talla(id_producto, talla_producto)
values (4, 'XS');
insert into Talla(id_producto, talla_producto)
values (4, 'S');
insert into Talla(id_producto, talla_producto)
values (4, 'M');
insert into Talla(id_producto, talla_producto)
values (4, 'L');
insert into Talla(id_producto, talla_producto)
values (4, 'XL');
insert into Talla(id_producto, talla_producto)
values (4, 'XXL');
insert into Talla(id_producto, talla_producto)
values (4, 'XXXL');
-- jean semilicrado
insert into Talla(id_producto, talla_producto)
values (5, '1'); 
insert into Talla(id_producto, talla_producto)
values (5, '2'); 
insert into Talla(id_producto, talla_producto)
values (5, '3'); 
insert into Talla(id_producto, talla_producto)
values (5, '4'); 
insert into Talla(id_producto, talla_producto)
values (5, '5'); 
insert into Talla(id_producto, talla_producto)
values (5, '6'); 
insert into Talla(id_producto, talla_producto)
values (5, '7'); 
insert into Talla(id_producto, talla_producto)
values (5, '8'); 
insert into Talla(id_producto, talla_producto)
values (5, '9'); 
insert into Talla(id_producto, talla_producto)
values (5, '10'); 
insert into Talla(id_producto, talla_producto)
values (5, '11'); 
insert into Talla(id_producto, talla_producto)
values (5, '12'); 
insert into Talla(id_producto, talla_producto)
values (5, '13'); 
insert into Talla(id_producto, talla_producto)
values (5, '14'); 
insert into Talla(id_producto, talla_producto)
values (5, '15'); 
insert into Talla(id_producto, talla_producto)
values (5, '16'); 


CREATE TABLE Color(
	id_producto           SERIAL ,
	id_talla              SERIAL ,
    color                 varchar(70),
	precio_producto       INTEGER ,
    CONSTRAINT fk_id_productoC FOREIGN KEY (id_producto) REFERENCES Producto (id_producto) ,
    CONSTRAINT fk_id_tallaC FOREIGN KEY (id_talla) REFERENCES Talla (id_talla) 
);
-- cartera de cuero
insert into Color(id_producto, id_talla, color, precio_producto)
values (1, 1,'azul',385);
insert into Color(id_producto, id_talla, color, precio_producto)
values (1, 1,'cafe',385);
insert into Color(id_producto, id_talla, color, precio_producto)
values (1, 2,'negro',385);
insert into Color(id_producto, id_talla, color, precio_producto)
values (1, 2,'gindo',385);
-- Botas Bucaneras
insert into Color(id_producto, id_talla, color, precio_producto)
values (2, 1,'negra',695);
insert into Color(id_producto, id_talla, color, precio_producto)
values (2, 1,'veige',695);
-- pantaletas
insert into Color(id_producto, id_talla, color, precio_producto)
values (3, 1,'rojo',125);
insert into Color(id_producto, id_talla, color, precio_producto)
values (3, 2,'negro',695);
insert into Color(id_producto, id_talla, color, precio_producto)
values (3, 3,'rosado',695);
insert into Color(id_producto, id_talla, color, precio_producto)
values (3, 1,'veige',695);
--Blusa Formal 
insert into Color(id_producto, id_talla, color, precio_producto)
values (4, 2,'Negro',285);
insert into Color(id_producto, id_talla, color, precio_producto)
values (4, 2,'Blanco',285);
insert into Color(id_producto, id_talla, color, precio_producto)
values (4, 3,'Gindo',285);
insert into Color(id_producto, id_talla, color, precio_producto)
values (4, 4,'veige',285);
-- jan semi licrado
insert into Color(id_producto, id_talla, color, precio_producto)
values (5, 1,'Negro',485);
insert into Color(id_producto, id_talla, color, precio_producto)
values (5, 1,'BLue Jeans',485);
insert into Color(id_producto, id_talla, color, precio_producto)
values (5, 1,'Blanco',485);

CREATE TABLE Promocion(
	id_promocion          SERIAL PRIMARY KEY ,
	nombre_promo         varchar(25) ,
	descripcion          varchar(250) ,
	url_banner_image      varchar(150) ,
	fecha_apertura        DATE ,
	fecha_conclucion      DATE 
);
insert into Promocion (id_promocion ,nombre_promo, descripcion,url_banner_image,fecha_apertura, fecha_conclucion)
values (0,'nula','','','2020/10/01','2020/11/15');

insert into Promocion (nombre_promo, descripcion,url_banner_image,fecha_apertura, fecha_conclucion)
values ('Temporada Hallowen','Reserva tus productos con descuentos de 10 20 y 30 % en productos de las diferentes marcas','https://lariviere.es/wp-content/uploads/2019/10/hallowen-la-riviere-2019.jpg','2020/10/01','2020/11/15');


CREATE TABLE carrito_add_produc_promo(
	id_carrito            SERIAL ,
	id_producto           SERIAL ,
	id_promocion          SERIAL ,
	cantidad_reservas     INTEGER ,
	fecha_limite          DATE ,
	fecha_inicio_reserva  DATE ,
    CONSTRAINT fk_id_carritoAdd FOREIGN KEY (id_carrito) REFERENCES Carrito (id_carrito) ,
    CONSTRAINT fk_id_productoAdd FOREIGN KEY (id_producto) REFERENCES Producto (id_producto) ,
    CONSTRAINT fk_id_promocionAdd FOREIGN KEY (id_promocion) REFERENCES Promocion (id_promocion) 
);

-- carrito 1 - 6
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (1, 2 ,1, 2,'2020/10/22','2020/10/25');
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (1, 3 ,0, 2,'2020/10/22','2020/10/25');
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (1, 4 ,1, 2,'2020/10/22','2020/10/25');

insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (2, 2 ,1, 2,'2020/10/22','2020/10/25');
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (2, 3 ,0, 2,'2020/10/22','2020/10/25');
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (2, 4 ,1, 2,'2020/10/22','2020/10/25');

insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (3, 2 ,1, 2,'2020/10/22','2020/10/25');
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (3, 3 ,0, 2,'2020/10/22','2020/10/25');
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (3, 4 ,1, 2,'2020/10/22','2020/10/25');

insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (4, 2 ,1, 2,'2020/10/22','2020/10/25');
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (4, 3 ,0, 2,'2020/10/22','2020/10/25');
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (4, 4 ,1, 2,'2020/10/22','2020/10/25');

insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (5, 2 ,1, 2,'2020/10/22','2020/10/25');
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (5, 3 ,0, 2,'2020/10/22','2020/10/25');
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (5, 4 ,1, 2,'2020/10/22','2020/10/25');

insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (6, 2 ,1, 2,'2020/10/22','2020/10/25');
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (6, 3 ,0, 2,'2020/10/22','2020/10/25');
insert into carrito_add_produc_promo (id_carrito, id_producto, id_promocion, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (6, 4 ,1, 2,'2020/10/22','2020/10/25');

CREATE TABLE carrito_add_producto(
    id_carrito            SERIAL,
    id_producto          SERIAL,
    cantidad              INTEGER,
    cantidad_reservas     INTEGER ,
	fecha_limite          DATE ,
	fecha_inicio_reserva  DATE ,
    CONSTRAINT fk_id_carritoAddp FOREIGN KEY (id_carrito) REFERENCES Carrito (id_carrito) ,
    CONSTRAINT fk_id_productoAddp FOREIGN KEY (id_producto) REFERENCES Producto (id_producto) 
);

insert into carrito_add_producto (id_carrito, id_producto, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (1, 4 ,1,'2020/10/22','2020/10/25');

insert into carrito_add_producto(id_carrito, id_producto, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (2, 3 ,6,'2020/10/22','2020/10/25');

insert into carrito_add_producto (id_carrito, id_producto, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (3, 2 ,5,'2020/10/22','2020/10/25');

insert into carrito_add_producto (id_carrito, id_producto, cantidad_reservas, fecha_limite, fecha_inicio_reserva)
values (4, 5 ,1,'2020/10/22','2020/10/25');

CREATE TABLE Promo_tiene_Productos(
	id_producto           SERIAL ,
	id_promocion          SERIAL ,
	stock                 INTEGER ,
	porcentaje_descuento  float ,
    CONSTRAINT fk_id_productoProm FOREIGN KEY (id_producto) REFERENCES Producto (id_producto) ,
    CONSTRAINT fk_id_promocionProm FOREIGN KEY (id_promocion) REFERENCES Promocion (id_promocion) 
);


insert into Promo_tiene_Productos (id_producto, id_promocion, stock,porcentaje_descuento)
values (1, 1,150,0.3);
insert into Promo_tiene_Productos (id_producto, id_promocion, stock,porcentaje_descuento)
values (2, 1,80,0.1);
insert into Promo_tiene_Productos (id_producto, id_promocion, stock,porcentaje_descuento)
values (3, 1,100,0.2);
insert into Promo_tiene_Productos (id_producto, id_promocion, stock,porcentaje_descuento)
values (4, 1,50,0.01);
insert into Promo_tiene_Productos (id_producto, id_promocion, stock,porcentaje_descuento)
values (5, 1,50,0.1);






